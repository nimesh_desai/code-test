# README #

### What is this repository for? ###

* This repo is for the code test for The Library for All.

### How do I get set up? ###

* Download this source as a zip or through git, load it up in Android Studio and you should be able to run it.
* Ensure that you've created the folder structure correctly as per the requirements, then update the URL of that location in PreferencesHelper.kt.

### Who do I talk to? ###

* If you run into any errors, please reach out.