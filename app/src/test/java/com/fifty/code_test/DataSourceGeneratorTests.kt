package com.fifty.code_test

import com.fifty.code_test.models.horizontal_scroll_view_with_title.Item
import com.fifty.code_test.models.horizontal_scroll_view_with_title.ItemType
import com.fifty.code_test.utility.data_source_generator.DataSourceGenerator
import org.junit.Assert
import org.junit.Test
import java.lang.reflect.Method

class DataSourceGeneratorTests {

    private fun getCreateItemMethod() : Method {
        val privateStringMethod: Method =
            DataSourceGenerator::class.java.getDeclaredMethod(
                "createItemWithDetails",
                String::class.java,
                String::class.java
            )

        privateStringMethod.isAccessible = true

        return privateStringMethod
    }

    @Test
    fun getExtension_Create_Item_Valid_PDF_Details() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "pdf"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.PDF)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Video_Details_AVI() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "avi"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.VIDEO)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Video_Details_MKV() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "mkv"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.VIDEO)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Video_Details_MOV() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "mov"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.VIDEO)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Video_Details_MP4() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "avi"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.VIDEO)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Video_Details_WEBM() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "webm"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.VIDEO)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Video_Details_WMMV() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = "wmv"
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item
        val expectedItem = Item(title, path, ext, ItemType.VIDEO)

        Assert.assertEquals(item, expectedItem)
    }

    @Test
    fun getExtension_Create_Item_Valid_Null_Extension() {
        val generator = DataSourceGenerator()

        val privateStringMethod = this.getCreateItemMethod()

        val ext = ""
        val title = "abc.$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item?

        Assert.assertEquals(item, null)
    }

    @Test
    fun getExtension_Create_Item_Valid_Invalid_Extension() {
        val generator = DataSourceGenerator()

        val privateStringMethod: Method =
            DataSourceGenerator::class.java.getDeclaredMethod(
                "createItemWithDetails",
                String::class.java,
                String::class.java
            )

        privateStringMethod.isAccessible = true

        val title = "abc.mp"
        val path = "/root/hdd/folder 1/abc.mp"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item?

        Assert.assertEquals(item, null)
    }

    @Test
    fun getExtension_Create_Item_Valid_Null_Filename() {
        val generator = DataSourceGenerator()

        val privateStringMethod: Method =
            DataSourceGenerator::class.java.getDeclaredMethod(
                "createItemWithDetails",
                String::class.java,
                String::class.java
            )

        privateStringMethod.isAccessible = true

        val ext = "mp4"
        val title = ".$ext"
        val path = "/root/hdd/folder 1/$title"
        val item =
            privateStringMethod.invoke(generator, title, path) as Item?

        Assert.assertEquals(item, null)
    }
}