package com.fifty.code_test

import com.fifty.code_test.models.horizontal_scroll_view_with_title.ItemType
import com.fifty.code_test.utility.string_utility.getExtension
import com.fifty.code_test.utility.string_utility.getTypeFromExtension
import org.junit.Test

import org.junit.Assert.*
import kotlin.math.exp

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class StringUtilityTests {
    @Test
    fun getExtension_When_PDF() {
        val testString = "test.pdf"
        assertEquals(testString.getExtension(), "pdf")
    }

    @Test
    fun getExtension_When_MPEG() {
        val testString = "test.mpeg"
        val expectedExtension = "mpeg"
        assertEquals(testString.getExtension(), expectedExtension)
    }

    @Test
    fun getExtension_When_Invalid_Length_Two() {
        val testString = "test.pd"
        val expectedExtension:String? = null
        assertEquals(testString.getExtension(), expectedExtension)
    }

    @Test
    fun getExtension_When_Invalid_No_Extension() {
        val testString = "test."
        val expectedExtension:String? = null
        assertEquals(testString.getExtension(), expectedExtension)
    }

    @Test
    fun getItemType_From_Extension_When_PDF() {
        val testExtension = "pdf"
        val expectedType = ItemType.PDF
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_AVI() {
        val testExtension = "avi"
        val expectedType = ItemType.VIDEO
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_MKV() {
        val testExtension = "mkv"
        val expectedType = ItemType.VIDEO
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_MOV() {
        val testExtension = "mov"
        val expectedType = ItemType.VIDEO
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_MP4() {
        val testExtension = "mp4"
        val expectedType = ItemType.VIDEO
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_WebM() {
        val testExtension = "webm"
        val expectedType = ItemType.VIDEO
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_WMV() {
        val testExtension = "wmv"
        val expectedType = ItemType.VIDEO
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }

    @Test
    fun getItemType_From_Extension_When_Invalid() {
        val testExtension = "xyz"
        val expectedType = ItemType.ERROR
        assertEquals(testExtension.getTypeFromExtension(), expectedType)
    }
}