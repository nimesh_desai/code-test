package com.fifty.code_test.utility.data_source_generator

import com.fifty.code_test.models.horizontal_scroll_view_with_title.HorizontalScrollWithTitleViewModel
import com.fifty.code_test.models.horizontal_scroll_view_with_title.Item
import com.fifty.code_test.models.horizontal_scroll_view_with_title.ItemType
import com.fifty.code_test.utility.string_utility.getExtension
import com.fifty.code_test.utility.string_utility.getTypeFromExtension
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File

interface DataSourceGeneratedListener {
    fun onDataSourceGenerated(dataSource: List<HorizontalScrollWithTitleViewModel>)
}

class DataSourceGenerator {

    fun generateDataSourceFromFolderLocation(location: String, listener: DataSourceGeneratedListener) {
        GlobalScope.launch(Dispatchers.IO) {

            val modelsList: ArrayList<HorizontalScrollWithTitleViewModel> = ArrayList()

            File(location).list()?.forEach {

                val model = generateModelFromFilesInFolder(location, it)

                if (model != null) {
                    modelsList.add(model)
                }
            }

            launch(Dispatchers.Main) {
                listener.onDataSourceGenerated(modelsList)
            }
        }
    }

    private fun generateModelFromFilesInFolder(parentLocation: String,
                                               subFolderName: String) : HorizontalScrollWithTitleViewModel? {

        val itemsList : ArrayList<Item> = ArrayList()

        File(parentLocation, subFolderName).listFiles()?.forEach {

            if (it.isDirectory == false) {
                val item = this.createItemWithDetails(it.name, it.absolutePath)
                if (item != null) {
                    itemsList.add(item)
                }
            }
        }

        if (itemsList.count() == 0) {
            return null
        }

        return HorizontalScrollWithTitleViewModel(subFolderName, itemsList)
    }

    private fun createItemWithDetails(name: String, path: String) : Item? {
        val ext = name.getExtension()
        val type = ext?.getTypeFromExtension()

        if (ext != null &&
            type != null &&
            type != ItemType.ERROR) {
            return Item(name, path, ext, type)
        }

        return null
    }
}