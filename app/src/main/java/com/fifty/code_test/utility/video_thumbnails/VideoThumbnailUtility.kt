package com.fifty.code_test.utility.video_thumbnails

import android.provider.MediaStore
import android.media.ThumbnailUtils
import android.graphics.Bitmap
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

interface VideoThumbnailCapturedNotificationListener {
    fun onThumbnailCapture(itemIndex:Int, thumbnail: Bitmap?)
}

class VideoThumbnailUtility {
    fun captureThumbnailFromVideo(absolutePath: String,
                                  itemIndex: Int,
                                  listener: VideoThumbnailCapturedNotificationListener) {

        GlobalScope.launch(Dispatchers.IO) {

            val thumbnail = ThumbnailUtils.createVideoThumbnail(absolutePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND)

            launch(Dispatchers.Main) {
                listener.onThumbnailCapture(itemIndex, thumbnail)
            }
        }
    }
}