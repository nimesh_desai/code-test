package com.fifty.code_test.utility.string_utility

import com.fifty.code_test.models.horizontal_scroll_view_with_title.ItemType
import com.fifty.code_test.models.horizontal_scroll_view_with_title.VideoType
import java.util.*

fun String.getExtension() : String? {
    // here the incoming file name is of the type
    // [something].[ext], with exactly 1 (one) "." (period) in the fileName
    // and at minimum, 3 characters in the extension

    val regex = Regex("^[\\w,\\s-]+\\.[a-zA-Z]{1}[a-zA-Z0-9]{2,}\$")

    if (this.matches(regex) == false) {
        return null
    }

    val parts = this.split(".")

    if (parts.count() != 2) {
        return null
    }

    return parts[1]
}

fun String.getTypeFromExtension() : ItemType {

    when (this) {
        ItemType.PDF.toString().toLowerCase(Locale.getDefault()) -> return ItemType.PDF
        VideoType.AVI.toString().toLowerCase(Locale.getDefault()) -> return ItemType.VIDEO
        VideoType.MKV.toString().toLowerCase(Locale.getDefault()) -> return ItemType.VIDEO
        VideoType.MOV.toString().toLowerCase(Locale.getDefault()) -> return ItemType.VIDEO
        VideoType.MP4.toString().toLowerCase(Locale.getDefault()) -> return ItemType.VIDEO
        VideoType.WEBM.toString().toLowerCase(Locale.getDefault()) -> return ItemType.VIDEO
        VideoType.WMV.toString().toLowerCase(Locale.getDefault()) -> return ItemType.VIDEO
    }

    return ItemType.ERROR
}