package com.fifty.code_test.utility.preferences_helper

import android.content.Context
import com.fifty.code_test.R

class PreferencesHelper(private val context: Context) {
    private var sharedPreferences = context.getSharedPreferences(context.getString(R.string.change_location_preference_file_key), Context.MODE_PRIVATE)

    fun getDataLocationURL() : String {
        val defaultFolderLocation = "/storage/emulated/0/library_for_all_code_test"
        val location = sharedPreferences.getString(context.getString(R.string.change_location_preference_key), defaultFolderLocation)

        if (location != null) {
            return location
        }

        return defaultFolderLocation
    }

    fun setLocation(location: String) {
        with (sharedPreferences.edit()) {
            putString(context.getString(R.string.change_location_preference_key), location)
            apply()
        }
    }
}
