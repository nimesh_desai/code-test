package com.fifty.code_test.views.horozontal_scroll_view_with_title_view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fifty.code_test.R
import com.fifty.code_test.models.horizontal_scroll_view_with_title.HorizontalScrollWithTitleViewModel
import com.fifty.code_test.models.horizontal_scroll_view_with_title.Item

interface HorizontalScrollWithTitleViewOnItemClickListener {
    fun onItemClicked(item: Item)
}

class HorizontalScrollWithTitleView (
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0,
    horizontalScrollWithTitleViewModel: HorizontalScrollWithTitleViewModel,
    horizontalScrollWithTitleViewOnItemClickListener: HorizontalScrollWithTitleViewOnItemClickListener
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    private val adapter: Adapter

    init {
        LayoutInflater.from(context).inflate(R.layout.horizontal_scroll_view_with_title, this, true)

        //update title
        this.updateTitle(horizontalScrollWithTitleViewModel.title)

        //update recycler view
        this.adapter = Adapter(horizontalScrollWithTitleViewModel, horizontalScrollWithTitleViewOnItemClickListener)
        this.updateRecyclerView(this.adapter)
    }

    @NonNull
    private fun updateTitle(title: String) {
        val titleTextView = findViewById<TextView>(R.id.horizontalRecyclerViewTitle)
        titleTextView.text = title
    }

    @NonNull
    private fun updateRecyclerView(adapter: Adapter) {
        val recyclerView = findViewById<RecyclerView>(R.id.horizontalRecyclerView)

        //Layout Manager
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView.layoutManager = layoutManager

        //Adapter
        recyclerView.adapter = adapter
    }

    //Public
    fun didLoadThumbnailForItemAtIndex(index: Int) {
        this.adapter.notifyItemChanged(index)
    }
}