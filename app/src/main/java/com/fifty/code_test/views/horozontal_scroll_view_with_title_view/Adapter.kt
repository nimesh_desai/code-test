package com.fifty.code_test.views.horozontal_scroll_view_with_title_view

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.fifty.code_test.R
import com.fifty.code_test.models.horizontal_scroll_view_with_title.HorizontalScrollWithTitleViewModel
import com.fifty.code_test.models.horizontal_scroll_view_with_title.Item


class Adapter(private val horizontalScrollWithTitleViewModel: HorizontalScrollWithTitleViewModel,
              private val horizontalScrollWithTitleViewOnItemClickListener: HorizontalScrollWithTitleViewOnItemClickListener
) : RecyclerView.Adapter<Adapter.HorizontalScrollViewHolder>() {

    companion object {
        @JvmStatic val MARGIN_NO_THUMBNAIL = 60
        @JvmStatic val MARGIN_THUMBNAIL = 0
    }

    //ViewHolder class
    class HorizontalScrollViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var imageView: ImageView = itemView.findViewById(R.id.recycler_view_item_file_type_image_view)

        private  fun updateImageViewLayoutMargins(margin: Int) {
            val layoutParams = this.imageView.layoutParams as LinearLayout.LayoutParams
            layoutParams.setMargins(margin, margin, margin, margin)
            this.imageView.layoutParams = layoutParams
        }

        @NonNull
        fun updateImage(resId: Int) {
            this.imageView.setImageResource(resId)

            this.updateImageViewLayoutMargins(MARGIN_NO_THUMBNAIL)
        }

        @NonNull
        fun updateImageWithThumbnail(thumbnail:Bitmap) {
            this.imageView.setImageBitmap(thumbnail)
            this.updateImageViewLayoutMargins(MARGIN_THUMBNAIL)
        }

        @NonNull
        fun bind(horizontalScrollWithTitleViewOnItemClickListener: HorizontalScrollWithTitleViewOnItemClickListener, item: Item) {
            itemView.setOnClickListener {
                horizontalScrollWithTitleViewOnItemClickListener.onItemClicked(item)
            }
        }
    }

    //conformance to Adapter methods
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HorizontalScrollViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.horizontal_scroll_view_item, parent, false)
        return HorizontalScrollViewHolder(itemView)
    }

    @NonNull
    override fun onBindViewHolder(holderHorizontalScrollViewHolder: HorizontalScrollViewHolder,
                                  position: Int) {
        val thumbnail = this.horizontalScrollWithTitleViewModel.getThumbnailAtIndex(position)
        if (thumbnail != null) {
            holderHorizontalScrollViewHolder.updateImageWithThumbnail(thumbnail)
        } else {
            holderHorizontalScrollViewHolder.updateImage(this.horizontalScrollWithTitleViewModel.getImageResIdForIndex(position))
        }

        val file = this.horizontalScrollWithTitleViewModel.getItemAtIndex(position)
        if (file != null) {
            holderHorizontalScrollViewHolder.bind(this.horizontalScrollWithTitleViewOnItemClickListener, file)
        }
    }

    override fun getItemCount(): Int {
        return this.horizontalScrollWithTitleViewModel.getItemCount()
    }
}