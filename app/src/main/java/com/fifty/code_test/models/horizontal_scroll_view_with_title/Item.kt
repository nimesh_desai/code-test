package com.fifty.code_test.models.horizontal_scroll_view_with_title

import android.graphics.Bitmap
import com.fifty.code_test.utility.string_utility.getExtension
import com.fifty.code_test.utility.string_utility.getTypeFromExtension

enum class ItemType {
    PDF, VIDEO, ERROR
}

enum class VideoType {
    AVI, MKV, MOV, MP4, WEBM, WMV
}

data class Item(val name: String,
                val path: String,
                val extension: String,
                val type: ItemType) {

    private var thumbnail: Bitmap? = null

    fun getThumbnail() : Bitmap? {
        return this.thumbnail
    }

    fun updateThumbnail(newThumbnail: Bitmap?) {
        this.thumbnail = newThumbnail
    }
}