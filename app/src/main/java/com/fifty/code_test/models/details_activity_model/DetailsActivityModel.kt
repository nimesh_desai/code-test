package com.fifty.code_test.models.details_activity_model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DetailsActivityModel(val name: String, val ext: String, val location: String) : Parcelable