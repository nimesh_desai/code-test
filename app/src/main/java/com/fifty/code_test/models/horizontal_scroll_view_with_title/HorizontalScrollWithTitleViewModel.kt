package com.fifty.code_test.models.horizontal_scroll_view_with_title

import android.graphics.Bitmap
import androidx.annotation.UiThread
import com.fifty.code_test.R
import com.fifty.code_test.utility.video_thumbnails.VideoThumbnailCapturedNotificationListener
import com.fifty.code_test.utility.video_thumbnails.VideoThumbnailUtility
import com.fifty.code_test.views.horozontal_scroll_view_with_title_view.HorizontalScrollWithTitleView

class HorizontalScrollWithTitleViewModel(val title: String,
                                         private val items: List<Item>) : VideoThumbnailCapturedNotificationListener {
    private var horizontalScrollWithTitleView:HorizontalScrollWithTitleView? = null

    init {
        this.generateThumbnailsForVideoItems()
    }

    private fun generateThumbnailsForVideoItems() {
        for ((index, item) in items.withIndex()) {
            VideoThumbnailUtility().captureThumbnailFromVideo(item.path, index, this)
        }
    }

    @UiThread
    override fun onThumbnailCapture(itemIndex:Int, thumbnail: Bitmap?) {
        if (itemIndex < this.items.count()) {
            val item = this.items[itemIndex]
            item.updateThumbnail(thumbnail)

            this.horizontalScrollWithTitleView?.didLoadThumbnailForItemAtIndex(itemIndex)
        }
    }

    //public
    fun getImageResIdForIndex(index: Int) : Int {
        if (index >= this.items.count()) {
            return 0 //Error
        }

        val file = this.items[index]

        return when (file.type) {
            ItemType.PDF -> R.mipmap.ic_pdf
            ItemType.VIDEO -> R.mipmap.ic_vid
            ItemType.ERROR -> 0
        }
    }

    fun getItemCount() : Int {
        return this.items.count()
    }

    fun getItemAtIndex(index: Int) : Item? {
        if (index >= this.items.count()) {
            return null //Error
        }

        return this.items[index]
    }

    fun getThumbnailAtIndex(index:Int) : Bitmap? {
        if (index >= this.items.count()) {
            return null //Error
        }

        return this.items[index].getThumbnail()
    }

    fun updateHorizontalScrollWithTitleView(view: HorizontalScrollWithTitleView) {
        this.horizontalScrollWithTitleView = view
    }
}