package com.fifty.code_test.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fifty.code_test.*
import com.fifty.code_test.models.details_activity_model.DetailsActivityModel
import com.fifty.code_test.utility.data_source_generator.DataSourceGenerator
import com.fifty.code_test.views.horozontal_scroll_view_with_title_view.HorizontalScrollWithTitleViewOnItemClickListener
import com.fifty.code_test.views.horozontal_scroll_view_with_title_view.HorizontalScrollWithTitleView
import com.fifty.code_test.models.horizontal_scroll_view_with_title.HorizontalScrollWithTitleViewModel
import com.fifty.code_test.models.horizontal_scroll_view_with_title.Item
import com.fifty.code_test.utility.data_source_generator.DataSourceGeneratedListener
import com.fifty.code_test.utility.preferences_helper.PreferencesHelper


class MainActivity : AppCompatActivity(), HorizontalScrollWithTitleViewOnItemClickListener, DataSourceGeneratedListener {

    private val READ_EXTERNAL_STORAGE_REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        this.setupReadExternalStoragePermissions()
    }

    private fun setupReadExternalStoragePermissions() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions (
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                READ_EXTERNAL_STORAGE_REQUEST_CODE
            )
        } else {
            this.setupMainView()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            READ_EXTERNAL_STORAGE_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    println("Permission has been denied by user")
                } else {
                    println("Permission has been granted by user")
                    this.setupMainView()
                }
            }
        }
    }

    private fun setupMainView() {
        //create horizontal scroll views based on folder details
        val locationURL = PreferencesHelper(this).getDataLocationURL()
        DataSourceGenerator().generateDataSourceFromFolderLocation(locationURL, this)
    }

    override fun onDataSourceGenerated(dataSource: List<HorizontalScrollWithTitleViewModel>) {
        //Main Linear Layout
        val mainLinearLayout = findViewById<LinearLayout>(R.id.main_linear_layout)
        mainLinearLayout.removeAllViews()

        val it = dataSource.iterator()
        while (it.hasNext()) {
            this.addHorizontalScrollViewToMainLinearLayout(mainLinearLayout, it.next())
        }
    }

    private fun addHorizontalScrollViewToMainLinearLayout (mainLinearLayout: LinearLayout,
                                                           viewModel: HorizontalScrollWithTitleViewModel) {
        val horizontalRecyclerView = HorizontalScrollWithTitleView (
            this,
            horizontalScrollWithTitleViewModel = viewModel,
            horizontalScrollWithTitleViewOnItemClickListener = this
        )

        viewModel.updateHorizontalScrollWithTitleView(horizontalRecyclerView)
        //add horizontal scroll view to main layout
        mainLinearLayout.addView(horizontalRecyclerView)
    }

    override fun onItemClicked(item: Item) {
        val detailsActivityModel = DetailsActivityModel(item.name, item.extension, item.path)
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.MODEL, detailsActivityModel)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.user_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.change_location_menu_item -> {
                this.showChangeLocationActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showChangeLocationActivity() {
        val intent = Intent(this, ChangeLocationActivity::class.java)
        startActivity(intent)
    }
}