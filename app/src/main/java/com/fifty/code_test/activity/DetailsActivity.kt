package com.fifty.code_test.activity

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.fifty.code_test.R
import com.fifty.code_test.models.details_activity_model.DetailsActivityModel

class DetailsActivity : AppCompatActivity() {

    companion object {
        @JvmStatic val MODEL = "MODEL"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        this.updateTextViews()
    }

    private fun updateTextViews() {
        val item: DetailsActivityModel? = intent.getParcelableExtra(MODEL)

        if (item != null) {
            val nameTextView = findViewById<TextView>(R.id.details_name_value)
            nameTextView.text = item.name

            val extTextView = findViewById<TextView>(R.id.details_ext_value)
            extTextView.text = item.ext

            val locationTextView = findViewById<TextView>(R.id.details_location_value)
            locationTextView.text = item.location
        }
    }
}