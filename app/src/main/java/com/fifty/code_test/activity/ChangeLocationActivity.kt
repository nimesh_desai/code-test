package com.fifty.code_test.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.fifty.code_test.R
import com.fifty.code_test.utility.preferences_helper.PreferencesHelper

class ChangeLocationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_location)

        this.updateEditTextView()
        this.addTextChangeListener()
    }

    private fun addTextChangeListener() {
        val editText = findViewById<EditText>(R.id.editText)
        editText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence,
                                       start: Int,
                                       before: Int,
                                       count: Int) {
                val newLocation = s.toString()
                updatePreferencesWithNewLocation(newLocation)
            }
        })
    }

    private fun updatePreferencesWithNewLocation(newLocation: String) {
        PreferencesHelper(this).setLocation(newLocation)
    }

    private fun updateEditTextView() {
        val editText = findViewById<EditText>(R.id.editText)
        editText.setText(PreferencesHelper(this).getDataLocationURL())
    }
}